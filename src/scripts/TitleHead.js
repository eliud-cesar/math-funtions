const TitleHead = Nametitle => {
    const HeadHtml = document.getElementById('metadataHead')
    const TitleHead = `
        <title>${Nametitle}</title>
    `
    HeadHtml.insertAdjacentHTML('afterbegin', TitleHead)
}

export default TitleHead