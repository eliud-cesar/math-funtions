import React from 'react';

const OutputOfTheSectionFigures = ({ ClassNameFigure }) => (
    <>
        <div id={`output-${ClassNameFigure}-area`} className="content-figures output">0</div>
        <div id={`output-${ClassNameFigure}-perimeter`} className="content-figures output">0</div>
    </>
)

export default OutputOfTheSectionFigures;