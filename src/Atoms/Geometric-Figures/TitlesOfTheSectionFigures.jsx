import React from 'react';

const TitleOfTheSectionFigures = () => (
    <>
        <div className="content-figures title">Área</div>
        <div className="content-figures title">Perimetro</div>
    </>
)

export default TitleOfTheSectionFigures;