import React from 'react';

const InputOfTheSectionFigures = ({ Placeholder, Funtion }) => (
    <input placeholder={Placeholder} type="number" className="content-figures input" onChange={Funtion} />
)

export default InputOfTheSectionFigures;