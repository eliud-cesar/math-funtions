import React from 'react';

const CanvasContainer = ({ TypeGraphic }) => (
    <div className={`container-${TypeGraphic}-graphic-output`} >
        <canvas className={`canvas-graphic ${TypeGraphic}`} id={`graphic-${TypeGraphic}-canvas`} ></canvas>
    </div>
)

export default CanvasContainer