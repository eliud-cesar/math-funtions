import React from 'react'

const TitleGraphic = ({ TypeGraphic, TextGraphic }) => (
    <h2 class={`title-graphic ${TypeGraphic}-graphic`}>Grafica {TextGraphic}</h2>
)

export default TitleGraphic;