// ** PAGINA DE CONVERTIR NOTACION ESTANDAR A CIENTIFICA Y VICEVERSA **

// import React, { Component } from 'react';
// import Img from '../Atoms/Ciencie/Img';
// import InformationCiencie from '../Molecules/Ciencie/InformationCiencie';

// class PageCiencie extends Component{
//     constructor(props) {
//         super(props)

//         this.state = {
//             InputValueNotationCiencie: "",
//             InputValueNotationCiencieExponentation: "",
//             InputValueNotationStandar: ""
//         }

//         this.SetStateInputNotationCiencie = this.SetStateInputNotationCiencie.bind(this)
//         this.SetStateInputNotationCiencieExponentation = this.SetStateInputNotationCiencieExponentation.bind(this)
//         this.SetStateInputNotationStandar = this.SetStateInputNotationStandar.bind(this)
//     }

//     SetStateInputNotationCiencie(e) {
//         const ValueNotationCiencie = e.target.value
//         this.setState({ InputValueNotationCiencie: ValueNotationCiencie })
//     }

//     SetStateInputNotationCiencieExponentation(e) {
//         const ValueNotationCiencieExponentation = e.target.value
//         this.setState({ InputValueNotationCiencieExponentation: ValueNotationCiencieExponentation })
//     }

//     SetStateInputNotationStandar(e) {
//         const ValueNotatioStandar = e.target.value
//         this.setState({ InputValueNotationStandar: ValueNotatioStandar })
//     }
    
//     componentDidMount() {
//         // const { InputValueNotationStandar } = this.state

//         // const cadena= "5024"
//         // const fstChar = cadena.charAt(0)
//         // console.log(fstChar)
//     }

//     // componentDidUpdate() {
//     //     const { InputValueNotationCiencie } = this.state
//     //     const { InputValueNotationCiencieExponentation } = this.state
//     //     const OutputNotationCiencie = document.getElementById('output-notation-standar')
//     // }

//     render() {
//         return (
//             <>
//             {/* <!-- TITLE PAGE --> */}
//             <h1 className="title-page-ciencie">Ciencia</h1>

//             {/* <!-- FIRST SECTION FROM CIENCE FOR STANDAR --> */}
//             <div className="section-notation-ciencie-for-standar">
//                 <div className="notation-ciencie">
//                     <h3>Notacion Cientifica</h3>
//                     <input type="number" className="input-notation-ciencie" onChange={this.SetStateInputNotationCiencie} />
//                 </div>
                
//                 <p className="notation-ciencie-x10">x10</p>
//                 <input type="number" className="input-notation-ciencie-exponentation" onChange={this.SetStateInputNotationCiencieExponentation} />
                
//                 {/* <!-- TOOLTIP --> */}
//                 <div className="tooltip active">
//                     <div className="content-tooltip">El exponente de la notacion</div>
//                     <div className="triangle"></div>
//                 </div>

//                 < Img ClassNum="1" />

//                 <div className="notation-standar">
//                     <h3>Notacion Estandar</h3>
//                     <p id="output-notation-standar" className="output-notation-standar"></p>
//                 </div>
//             </div>

//             <div className="separation-section"></div>

//             {/* <!-- SECOND SECTION FROM STANDAR FOR CIENCIE --> */}
//             <div className="section-notation-standar-for-ciencie">
//                 <div className="notation-standar">
//                     <h3>Notacion Estandar</h3>
//                     <input type="number" className="input-notation-ciencie" onChange={this.SetStateInputNotationStandar} />
//                 </div>
                
                
//                 < Img ClassNum="2" />
                
//                 <div className="notation-ciencie">
//                     <h3>Notacion Cientifica</h3>
//                     <p id="output-notation-ciencie" className="output-notation-ciencie"><sup></sup></p>
//                 </div>
//             </div>

//             {/* <!-- INFORMATION --> */}
//             < InformationCiencie />
//             </>
//         )
//     }
// }

// export default PageCiencie;