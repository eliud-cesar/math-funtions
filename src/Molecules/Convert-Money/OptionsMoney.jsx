import React from 'react'

const OptionMoney = () => (
    <>
        <option value="" disabled selected>Seleccione una Moneda...</option>
        <option value="1">USD (dolar)</option>
        <option value="2">MXN (peso mexicano)</option>
        <option value="3">YEN (peso japonesa)</option>
        <option value="4">EURO (peso español)</option>
        <option value="5">SOL (peso peruano)</option>
    </>
)

export default OptionMoney;