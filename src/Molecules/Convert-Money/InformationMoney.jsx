import React from 'react';

const InformationMoney = ({ Title, Description, ImageUrl }) => (
    <div class="card-information-money">
        <h2> {Title} </h2>
        <p> {Description} </p>
        <img src={ImageUrl} alt={Title} />
    </div>
)

export default InformationMoney;