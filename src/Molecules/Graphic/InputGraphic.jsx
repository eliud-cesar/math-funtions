import React from 'react';

const InputGraphic = ({ TypeGraphic, btn , inpV1, inpV2, inpV3, inpV4, inpV5, inpV6, inpV7, inpV8, inpT1, inpT2, inpT3, inpT4, inpT5, inpT6, inpT7, inpT8 }) => (
    <div class={`container-input-data-${TypeGraphic}`}>

        <input type="text" placeholder="Title 1" onChange={inpT1} />
        <input type="text" placeholder="Title 2" onChange={inpT2} />
        <input type="text" placeholder="Title 3" onChange={inpT3} />
        <input type="text" placeholder="Title 4" onChange={inpT4} />

        <input type="number" name="" id="" placeholder="1" onChange={inpV1} />
        <input type="number" name="" id="" placeholder="2" onChange={inpV2} />
        <input type="number" name="" id="" placeholder="3" onChange={inpV3} />
        <input type="number" name="" id="" placeholder="4" onChange={inpV4} />
        
        <input type="text" placeholder="Title 5" onChange={inpT5} />
        <input type="text" placeholder="Title 6" onChange={inpT6} />
        <input type="text" placeholder="Title 7" onChange={inpT7} />
        <input type="text" placeholder="Title 8" onChange={inpT8} />
        
        <input type="number" name="" id="" placeholder="5" onChange={inpV5} />
        <input type="number" name="" id="" placeholder="6" onChange={inpV6} />
        <input type="number" name="" id="" placeholder="7" onChange={inpV7} />
        <input type="number" name="" id="" placeholder="8" onChange={inpV8} />
        
        <button className={`button-submit-create-graphic ${TypeGraphic}`} onClick={btn} >Crear la grafica</button>

    </div>
)

export default InputGraphic;