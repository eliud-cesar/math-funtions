import React from 'react';

const Footer = () => (
    <footer>
        <div className="mathfuntions container-footer">
            <div className="container-content-footer">
                <h2 className="title-social-media">Redes Sociales</h2>

                <p><a target="_blank" href="https://instagram.com/eliud__cesar">Instagram</a></p>
                <p><a target="_blank" href="https://linkedin.com/in/eliud-cesar">LinkedIn</a></p>
                <p><a target="_blank" href="https://github.com/eliud-cesar">Github</a></p>
            </div>
            
            <div className="container-content-footer">
                <h2 className="title-about-me">About me</h2>

                <p><a target="_blank" className="portfolio" href="https://eliudcesar.netlify.app/">eliud-cesar</a></p>
                <p><a target="_blank" href="https://gitlab.com/eliud-cesar/math-funtions">Codigo - Gitlab</a></p>
            </div>
            
            <div className="container-content-footer">
                <h2 className="title-information">Información</h2>

                <p>Esta web esta hecha con fines educativos, practicos y de apoyo para los que puedan aprovechar esta web.</p>
            </div>
        </div>
    </footer>
)

export default Footer;