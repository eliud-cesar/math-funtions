import React from 'react';
import PropTypes from 'prop-types';

const CardMasterPresentacion = ({ Image, Link, Title, Description }) => (
    <div class="card-master-presentation-funtion 2">
        <a href={Link}><h2>{ Title }</h2></a>
        
        <img src={ Image } alt={ Title } />
        
        <p>{ Description }</p>
    </div>
)

CardMasterPresentacion.propTypes = {
    Image: PropTypes.string,
    Link: PropTypes.string,
    Title: PropTypes.string,
    Description: PropTypes.string
}
CardMasterPresentacion.defaultProps = {
    Image: "https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/master/anime.jpg",
    Link: "/error",
    Title: "Title not found",
    Description: "Description not found"
}

export default CardMasterPresentacion;