import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import TitleAlt from '../Atoms/TitleAlt'

// LINKS OF THE ICONS USED
const LinkIsopo = "https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/672c238e03fc763769ae2f3cc24210ad588dec68/Icons/Isopo.svg" 
const LinkMenuHamburguesa = "https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/672c238e03fc763769ae2f3cc24210ad588dec68/Icons/menu-hamburguesa.svg"

class Header extends Component {
    constructor(props) {
        super(props)
    }

    // FUNCTION TO ACTIVATE THE LINKS MENU, TO MAKE IT VISIBLE AND ALSO THE ROTATION ANIMATION OF THE ICON MENU
    funtionActiveMain() {
        const menuHeader = document.getElementById('container-funtions-header')
        const iconMenuHamburguesa = document.getElementById('icon-menu-h')

        menuHeader.classList.toggle('active')
        iconMenuHamburguesa.classList.toggle('rotation')
    }

    render() {
        return(
    <header>
        <div className="mathfuntions container-content-header">
            <NavLink className="isopo-header" to="/">
                <img src={LinkIsopo} alt={TitleAlt} className="isopo-header"/>
            </NavLink>
            
            <div className="menu-hamburguesa" onClick={this.funtionActiveMain}>
                <img id="icon-menu-h" src={LinkMenuHamburguesa} alt={TitleAlt} className="isopo-header" />
            </div>
            
            <div id="container-funtions-header" className="container-funtions-header">
                <p><NavLink to="/operaciones-matematicas">Matematicas</NavLink></p>
                <p><NavLink to="/convertir-dinero">Convertir dinero</NavLink></p>
                <p><NavLink to="/figuras-geometricas">Figuras geometricas</NavLink></p>
                <p><NavLink to="/grafica">Grafica</NavLink></p>
            </div>
        </div>
    </header>
        )
    }
}

export default Header;