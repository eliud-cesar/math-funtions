import React from 'react';

const InformationCiencie = () => (
    <div className="container-information-page-ciencie">
        <div className="content-page-ciencie">
            <h2>Notacion Cientifica</h2>
            <p>La notación científica, también denominada notación en forma exponencial, es una forma de escribir los números que acomoda valores demasiado grandes (100 000 000 000) o pequeños como puede ser el siguiente (0.000 000 000 01)1​para ser escrito de manera convencional.2​3​ El uso de esta notación se basa en potencias de 104​ (los casos ejemplificados anteriormente en notación científica, quedarían 1 × 1011 y 1 × 10−11, respectivamente). El módulo del exponente en el caso anterior es la cantidad de ceros que lleva el número delante, en caso de ser negativo (nótese que el cero delante de la coma también cuenta), o detrás, en caso de tratarse de un exponente positivo.</p>
        </div>

        <div className="content-page-ciencie">
            <h2>Notacion Estandar</h2>
            <p>La notación científica (o notación índice estándar) es una manera rápida de representar un número utilizando potencias de base diez. Esta notación se utiliza para poder expresar muy fácilmente números muy grandes o muy pequeños.</p>
        </div>
    </div>
)

export default InformationCiencie;