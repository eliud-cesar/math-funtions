import React from 'react';

const Information = ({Title, Description}) => (
    <>
        <h3>{ Title }</h3>
        <p>{ Description }</p>
    </>
)

export default Information;