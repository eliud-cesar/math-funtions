import axios from 'axios';
import React, { Component } from 'react';
import OptionMoney from '../Molecules/Convert-Money/OptionsMoney';
import InformationMoneyApi from '../Organisms/Convert-Money/InformationMoneyApi';
import TitleHead from '../scripts/TitleHead';

class PageConvertirDinero extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // THE STATIC API
            dataApiInitia: [],
            // DYNAMIC INFORMATION API
            dataInformationOne: [],
            dataInformationTwo: [],
            // SELECTED COINS
            moneyNationalOne: "",
            moneyNationalTwo: "",
            // INTRODUCED VALUE
            valueInputMoney: ""
        }

        // METHODS
        this.ValueOptionOne = this.ValueOptionOne.bind(this)
        this.ValueOptionTwo = this.ValueOptionTwo.bind(this)
        this.ValueInputMoney = this.ValueInputMoney.bind(this)
        this.ConvertMoney = this.ConvertMoney.bind(this)
    }

    componentDidMount() {
        {axios.get("https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/master/Apis/Covert-Money.json")
            .then(resp => {
                this.setState({
                    dataApiInitia: resp.data
                })
            })}

        // REMOVE THE MESSAJE BETA
        {
                const MessajeBeta = document.getElementById("messaje-beta-container")
                MessajeBeta.classList.remove("active")
        }

        TitleHead("Convertir Dinero")
    }

    // METHOD THAT EXECUTES THE CONVERSION
    ConvertMoney() {
        // SELECTED COINS
        const { moneyNationalOne } = this.state
        const { moneyNationalTwo } = this.state
        // INTRODUCED VALUE
        const { valueInputMoney } = this.state
        // COINTAINER OUTPUT
        const Output = document.getElementById('output-money')
        
        // METHOD THAT VALIDATES EACH SELECTION AND PRINTS IT CONVERTED
        {
        // USD
        (moneyNationalOne === "USD" && moneyNationalTwo === "MXN")   ? Output.innerHTML = valueInputMoney * 20     : 
        (moneyNationalOne === "USD" && moneyNationalTwo === "YEN")   ? Output.innerHTML = valueInputMoney * 109    : 
        (moneyNationalOne === "USD" && moneyNationalTwo === "EURO")  ? Output.innerHTML = valueInputMoney * 0.82   :
        (moneyNationalOne === "USD" && moneyNationalTwo === "SOL")   ? Output.innerHTML = valueInputMoney * 3.74   :
        // MXN
        (moneyNationalOne === "MXN" && moneyNationalTwo === "USD")   ? Output.innerHTML = valueInputMoney * 0.050  :
        (moneyNationalOne === "MXN" && moneyNationalTwo === "YEN")   ? Output.innerHTML = valueInputMoney * 5.40   :
        (moneyNationalOne === "MXN" && moneyNationalTwo === "EURO")  ? Output.innerHTML = valueInputMoney * 0.041  :
        (moneyNationalOne === "MXN" && moneyNationalTwo === "SOL")   ? Output.innerHTML = valueInputMoney * 0.19   :
        // YEN
        (moneyNationalOne === "YEN" && moneyNationalTwo === "MXN")   ? Output.innerHTML = valueInputMoney * 0.18   :
        (moneyNationalOne === "YEN" && moneyNationalTwo === "USD")   ? Output.innerHTML = valueInputMoney * 0.0092 :
        (moneyNationalOne === "YEN" && moneyNationalTwo === "EURO")  ? Output.innerHTML = valueInputMoney * 0.0075 :
        (moneyNationalOne === "YEN" && moneyNationalTwo === "SOL")   ? Output.innerHTML = valueInputMoney * 0.034  :
        // EURO
        (moneyNationalOne === "EURO" && moneyNationalTwo === "MXN")  ? Output.innerHTML = valueInputMoney * 24.28  :
        (moneyNationalOne === "EURO" && moneyNationalTwo === "YEN")  ? Output.innerHTML = valueInputMoney * 133.18 :
        (moneyNationalOne === "EURO" && moneyNationalTwo === "USD")  ? Output.innerHTML = valueInputMoney * 1.22   :
        (moneyNationalOne === "EURO" && moneyNationalTwo === "SOL")  ? Output.innerHTML = valueInputMoney * 4.57   :
        // SOLES
        (moneyNationalOne === "SOL" && moneyNationalTwo === "MXN")   ? Output.innerHTML = valueInputMoney * 5.31   :
        (moneyNationalOne === "SOL" && moneyNationalTwo === "YEN")   ? Output.innerHTML = valueInputMoney * 29.12  :
        (moneyNationalOne === "SOL" && moneyNationalTwo === "EURO")  ? Output.innerHTML = valueInputMoney * 0.22   :
        (moneyNationalOne === "SOL" && moneyNationalTwo === "USD")   ? Output.innerHTML = valueInputMoney * 0.27   :
        // REPEAT
        (moneyNationalOne === "USD" && moneyNationalTwo === "USD")   ? Output.innerHTML = "REPEAT USD"             :
        (moneyNationalOne === "MXN" && moneyNationalTwo === "MXN")   ? Output.innerHTML = "REPEAT MXN"             :
        (moneyNationalOne === "YEN" && moneyNationalTwo === "YEN")   ? Output.innerHTML = "REPEAT YEN"             :
        (moneyNationalOne === "EURO" && moneyNationalTwo === "EURO") ? Output.innerHTML = "REPEAT EURO"            :
        (moneyNationalOne === "SOL" && moneyNationalTwo === "SOL")   ? Output.innerHTML = "REPEAT SOL"             :
        console.log("NINGUNO DE LOS CASOS ANTERIORES !!!")
        }
    }

    // METHOD THAT UPDATES THE STATUS OF THE SECURITY THAT IS INTRODUCED TO CONVERT
    ValueInputMoney(e) {
        const InputMoney = e.target.value
        this.setState({ valueInputMoney: InputMoney })
    }

    // METHOD TO UPDATE THE STATUS OF THE NATIONAL CURRENCY OPTION OF THE FIRST OPTION
    ValueOptionOne(e) {
        let index = e.target.selectedIndex;
        const ValueOption = e.target.options[index].value

        const { dataApiInitia } = this.state

        switch (ValueOption) {
            // USD
            case '1':
                const InformationUsd = dataApiInitia[0]
                this.setState ({ dataInformationOne: [InformationUsd], moneyNationalOne: "USD" })
                break
            // MXN
            case '2':
                const InformationMxn = dataApiInitia[1]
                this.setState ({ dataInformationOne: [InformationMxn], moneyNationalOne: "MXN" })

                break
            // YEN
            case '3':
                const InformationYen = dataApiInitia[2]
                this.setState ({ dataInformationOne: [InformationYen], moneyNationalOne: "YEN" })

                break
            // EURO
            case '4': 
            const InformationEuro = dataApiInitia[3]
            this.setState ({ dataInformationOne: [InformationEuro], moneyNationalOne: "EURO" })

                break
            // SOL
            case '5':
                const InformationSol = dataApiInitia[4]
                this.setState ({ dataInformationOne: [InformationSol], moneyNationalOne: "SOL" })

                break
        }
    }

    // METHOD TO UPDATE THE STATUS OF THE NATIONAL CURRENCY OPTION OF THE SECOND OPTION
    ValueOptionTwo(e) {
        let index = e.target.selectedIndex;
        const ValueOption = e.target.options[index].value

        const { dataApiInitia } = this.state

        switch (ValueOption) {
            // USD
            case '1':
                const InformationUsd = dataApiInitia[0]
                this.setState ({ dataInformationTwo: [InformationUsd], moneyNationalTwo: "USD" })

                break
            // MXN
            case '2':
                const InformationMxn = dataApiInitia[1]
                this.setState ({ dataInformationTwo: [InformationMxn], moneyNationalTwo: "MXN" })

                break
            // YEN
            case '3':
                const InformationYen = dataApiInitia[2]
                this.setState ({ dataInformationTwo: [InformationYen], moneyNationalTwo: "YEN" })

                break
            // EURO
            case '4': 
                const InformationEuro = dataApiInitia[3]
                this.setState ({ dataInformationTwo: [InformationEuro], moneyNationalTwo: "EURO" })
                
                break
            // SOL
            case '5':
                const InformationSol = dataApiInitia[4]
                this.setState ({ dataInformationTwo: [InformationSol], moneyNationalTwo: "SOL" })
        }
    }

    render() {
        // DATA INFORMATION
        const { dataInformationOne } = this.state
        const { dataInformationTwo } = this.state

        return(
            <>
            {/* <!-- TITLE --> */}
            <h1 class="title-money-convert">Convertir Pesos</h1>
    
            {/* <!-- OPTIONS --> */}
            <div class="first-section-convert">
                {/* <!-- FIRSTS SELECTIONS --> */}
                <div class="container-select-convert-money one">
                    <select onChange={this.ValueOptionOne} >
                        < OptionMoney />
                    </select>
                </div>

                {/* <!-- SECONDS SELECTIONS --> */}
                <div class="container-select-convert-money two">
                    <select onChange={this.ValueOptionTwo} >
                        < OptionMoney />
                    </select>
                </div>
            </div>
            
            {/* <!-- INPUTS --> */}
            <div class="second-section-convert">
                {/* <!-- INPUT MONEY LAN --> */}
                <div class="container-input-money one">
                    <input id="input-convert-money" onChange={this.ValueInputMoney} type="number" />
                </div>
                {/* <!-- OUPUT MONEY CONVERT --> */}
                <div class="container-input-money two">
                    <p>$ <span id="output-money"></span></p>
                </div>
            </div>

            {/* BUTTON */}
            <div className="container-button-start-operation m-t-1rem" >
                <input id="button-test" type="submit" value="Convertir" onClick={this.ConvertMoney} />
            </div>
            
            {/* <!-- INFORMATIONS --> */}
                < InformationMoneyApi ApiInformationOne={dataInformationOne} ApiInformationTwo={dataInformationTwo} />
            </>
        )
    }
}

export default PageConvertirDinero;