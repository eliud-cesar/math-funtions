import Chart from 'chart.js/auto';
import { Component } from 'react';
import CanvasContainer from '../Atoms/Graphics/CanvasContainer';
import InputGraphic from '../Molecules/Graphic/InputGraphic';
import TitleGraphic from '../Atoms/Graphics/TitleGraphic';
import TitleHead from '../scripts/TitleHead';

class PageGraphic extends Component {
    constructor({props, watch}) {
        super({props, watch})

        this.state = {
            // VALUE GRAPHIC BAR
            ValueBarOne   : "",
            ValueBarTwo   : "",
            ValueBarThree : "",
            ValueBarFour  : "",
            ValueBarFive  : "",
            ValueBarSix   : "",
            ValueBarSeven : "",
            ValueBarEight : "",
            // TITLE GRAPHIC BAR
            TitleBarOne   : "",
            TitleBarTwo   : "",
            TitleBarThree : "",
            TitleBarFour  : "",
            TitleBarFive  : "",
            TitleBarSix   : "",
            TitleBarSeven : "",
            TitleBarEight : "",
            // ----------------------------
            // VALUE GRAPHIC CIRCLE
            ValueCircleOne   : "",
            ValueCircleTwo   : "",
            ValueCircleThree : "",
            ValueCircleFour  : "",
            ValueCircleFive  : "",
            ValueCircleSix   : "",
            ValueCircleSeven : "",
            ValueCircleEight : "",
            // TITLE GRAPHIC CIRCLE
            TitleCircleOne   : "",
            TitleCircleTwo   : "",
            TitleCircleThree : "",
            TitleCircleFour  : "",
            TitleCircleFive  : "",
            TitleCircleSix   : "",
            TitleCircleSeven : "",
            TitleCircleEight : ""
        }

{        // BIND FOR VALUE GRAPHIC BAR
        this.UpdateValueBarOne   = this.UpdateValueBarOne.bind(this)
        this.UpdateValueBarTwo   = this.UpdateValueBarTwo.bind(this)
        this.UpdateValueBarThree = this.UpdateValueBarThree.bind(this)
        this.UpdateValueBarFour  = this.UpdateValueBarFour.bind(this)
        this.UpdateValueBarFive  = this.UpdateValueBarFive.bind(this)
        this.UpdateValueBarSix   = this.UpdateValueBarSix.bind(this)
        this.UpdateValueBarSeven = this.UpdateValueBarSeven.bind(this)
        this.UpdateValueBarEight = this.UpdateValueBarEight.bind(this)
        // BIND FOR TITLE GRAPHIC BAR
        this.UpdateTitleBarOne   = this.UpdateTitleBarOne.bind(this)
        this.UpdateTitleBarTwo   = this.UpdateTitleBarTwo.bind(this)
        this.UpdateTitleBarThree = this.UpdateTitleBarThree.bind(this)
        this.UpdateTitleBarFour  = this.UpdateTitleBarFour.bind(this)
        this.UpdateTitleBarFive  = this.UpdateTitleBarFive.bind(this)
        this.UpdateTitleBarSix   = this.UpdateTitleBarSix.bind(this)
        this.UpdateTitleBarSeven = this.UpdateTitleBarSeven.bind(this)
        this.UpdateTitleBarEight = this.UpdateTitleBarEight.bind(this)
        // ---------------------------------------------------------------------
        // BIND FOR VALUE GRAPHIC BAR
        this.UpdateValueCircleOne   = this.UpdateValueCircleOne.bind(this)
        this.UpdateValueCircleTwo   = this.UpdateValueCircleTwo.bind(this)
        this.UpdateValueCircleThree = this.UpdateValueCircleThree.bind(this)
        this.UpdateValueCircleFour  = this.UpdateValueCircleFour.bind(this)
        this.UpdateValueCircleFive  = this.UpdateValueCircleFive.bind(this)
        this.UpdateValueCircleSix   = this.UpdateValueCircleSix.bind(this)
        this.UpdateValueCircleSeven = this.UpdateValueCircleSeven.bind(this)
        this.UpdateValueCircleEight = this.UpdateValueCircleEight.bind(this)
        // BIND FOR TITLE GRAPHIC BAR
        this.UpdateTitleCircleOne   = this.UpdateTitleCircleOne.bind(this)
        this.UpdateTitleCircleTwo   = this.UpdateTitleCircleTwo.bind(this)
        this.UpdateTitleCircleThree = this.UpdateTitleCircleThree.bind(this)
        this.UpdateTitleCircleFour  = this.UpdateTitleCircleFour.bind(this)
        this.UpdateTitleCircleFive  = this.UpdateTitleCircleFive.bind(this)
        this.UpdateTitleCircleSix   = this.UpdateTitleCircleSix.bind(this)
        this.UpdateTitleCircleSeven = this.UpdateTitleCircleSeven.bind(this)
        this.UpdateTitleCircleEight = this.UpdateTitleCircleEight.bind(this)}
        // ---------------------------------------------------------------------
        // BIND FOR CREATE GRAPHIC 
        this.CreateGraphicStandar = this.CreateGraphicStandar.bind(this)
        this.CreateGraphicCircle = this.CreateGraphicCircle.bind(this)
    }

    // ACTIVE THE MESSAJE BETA
    componentDidMount() {
        const MessajeBeta = document.getElementById("messaje-beta-container")
        MessajeBeta.classList.add("active")

        TitleHead("Graficas")
    }

    // CREATE THE GRAPHIC STANDAR
    CreateGraphicStandar() {
        // VALUE
        const { ValueBarOne } = this.state; const { ValueBarTwo } = this.state
        const { ValueBarThree } = this.state; const { ValueBarFour } = this.state;
        const { ValueBarFive } = this.state; const { ValueBarSix } = this.state;
        const { ValueBarSeven } = this.state; const { ValueBarEight } = this.state;
        // TITLE
        const { TitleBarOne } = this.state; const { TitleBarTwo } = this.state;
        const { TitleBarThree } = this.state; const { TitleBarFour } = this.state;
        const { TitleBarFive } = this.state; const { TitleBarSix } = this.state;
        const { TitleBarSeven } = this.state; const { TitleBarEight } = this.state;
        
        const ctx = document.getElementById('graphic-standar-canvas').getContext('2d')
        const ChartJs = new Chart(ctx, {
            type: "bar",
            data:{
                labels: [TitleBarOne, TitleBarTwo, TitleBarThree, TitleBarFour, TitleBarFive, TitleBarSix, TitleBarSeven, TitleBarEight],
                datasets:[{
                    label: "Grafica Estandar",
                    data: [ValueBarOne, ValueBarTwo, ValueBarThree, ValueBarFour, ValueBarFive, ValueBarSix, ValueBarSeven, ValueBarEight],
                    backgroundColor: [
                        'rgb(0, 123, 223)',
                        'rgb(255, 60, 50)',
                        'rgb(251, 169, 5)',
                        'rgb(46, 189, 117)',
                        'rgb(0, 109, 198)',
                        'rgb(250, 253, 255)',
                        'rgb(40, 45, 49)',
                        'rgb(20, 22, 24)'
                    ]
                }]
            },
            options:{
                scales:{
                    y: {
                        beginAtZero: true
                    }
                }
            }
        })
    }

    // CREATE THE GRAPHIC CIRCLE
    CreateGraphicCircle() {
        // VALUE
        const { ValueCircleOne } = this.state; const { ValueCircleTwo } = this.state
        const { ValueCircleThree } = this.state; const { ValueCircleFour } = this.state;
        const { ValueCircleFive } = this.state; const { ValueCircleSix } = this.state;
        const { ValueCircleSeven } = this.state; const { ValueCircleEight } = this.state;
        // TITLE
        const { TitleCircleOne } = this.state; const { TitleCircleTwo } = this.state;
        const { TitleCircleThree } = this.state; const { TitleCircleFour } = this.state;
        const { TitleCircleFive } = this.state; const { TitleCircleSix } = this.state;
        const { TitleCircleSeven } = this.state; const { TitleCircleEight } = this.state;
        
        const ctx = document.getElementById('graphic-circle-canvas').getContext('2d')
        const ChartJs = new Chart(ctx, {
            type: "doughnut",
            data:{
                labels: [TitleCircleOne, TitleCircleTwo, TitleCircleThree, TitleCircleFour, TitleCircleFive, TitleCircleSix, TitleCircleSeven, TitleCircleEight],
                datasets:[{
                    label: "Grafica Circular",
                    data: [ValueCircleOne, ValueCircleTwo, ValueCircleThree, ValueCircleFour, ValueCircleFive, ValueCircleSix, ValueCircleSeven, ValueCircleEight],
                    backgroundColor: [
                        'rgb(40, 45, 49)',
                        'rgb(251, 169, 5)',
                        'rgb(0, 123, 223)',
                        'rgb(255, 60, 50)',
                        'rgb(20, 22, 24)',
                        'rgb(250, 253, 255)',
                        'rgb(46, 189, 117)',
                        'rgb(0, 109, 198)'
                    ]
                }]
            }
        }
    )}

    // UPDATE VALUE GRAPHIC BAR
    UpdateValueBarOne(e)   { let ValueInput = e.target.value; this.setState({ ValueBarOne   : ValueInput }) }
    UpdateValueBarTwo(e)   { let ValueInput = e.target.value; this.setState({ ValueBarTwo   : ValueInput }) }
    UpdateValueBarThree(e) { let ValueInput = e.target.value; this.setState({ ValueBarThree : ValueInput }) }
    UpdateValueBarFour(e)  { let ValueInput = e.target.value; this.setState({ ValueBarFour  : ValueInput }) }
    UpdateValueBarFive(e)  { let ValueInput = e.target.value; this.setState({ ValueBarFive  : ValueInput }) }
    UpdateValueBarSix(e)   { let ValueInput = e.target.value; this.setState({ ValueBarSix   : ValueInput }) }
    UpdateValueBarSeven(e) { let ValueInput = e.target.value; this.setState({ ValueBarSeven : ValueInput }) }
    UpdateValueBarEight(e) { let ValueInput = e.target.value; this.setState({ ValueBarEight : ValueInput }) }
    // UPDATE TITLE GRAPHIC BAR
    UpdateTitleBarOne(e)   { let TitleInput = e.target.value; this.setState({ TitleBarOne   : TitleInput }) }
    UpdateTitleBarTwo(e)   { let TitleInput = e.target.value; this.setState({ TitleBarTwo   : TitleInput }) }
    UpdateTitleBarThree(e) { let TitleInput = e.target.value; this.setState({ TitleBarThree : TitleInput }) }
    UpdateTitleBarFour(e)  { let TitleInput = e.target.value; this.setState({ TitleBarFour  : TitleInput }) }
    UpdateTitleBarFive(e)  { let TitleInput = e.target.value; this.setState({ TitleBarFive  : TitleInput }) }
    UpdateTitleBarSix(e)   { let TitleInput = e.target.value; this.setState({ TitleBarSix   : TitleInput }) }
    UpdateTitleBarSeven(e) { let TitleInput = e.target.value; this.setState({ TitleBarSeven : TitleInput }) }
    UpdateTitleBarEight(e) { let TitleInput = e.target.value; this.setState({ TitleBarEight : TitleInput }) }
    // -------------------------------------------------------------------------------------------------------
    // UPDATE VALUE GRAPHIC CIRCLE
    UpdateValueCircleOne(e)   { let ValueInput = e.target.value; this.setState({ ValueCircleOne   : ValueInput}) }
    UpdateValueCircleTwo(e)   { let ValueInput = e.target.value; this.setState({ ValueCircleTwo   : ValueInput}) }
    UpdateValueCircleThree(e) { let ValueInput = e.target.value; this.setState({ ValueCircleThree : ValueInput}) }
    UpdateValueCircleFour(e)  { let ValueInput = e.target.value; this.setState({ ValueCircleFour  : ValueInput}) }
    UpdateValueCircleFive(e)  { let ValueInput = e.target.value; this.setState({ ValueCircleFive  : ValueInput}) }
    UpdateValueCircleSix(e)   { let ValueInput = e.target.value; this.setState({ ValueCircleSix   : ValueInput}) }
    UpdateValueCircleSeven(e) { let ValueInput = e.target.value; this.setState({ ValueCircleSeven : ValueInput}) }
    UpdateValueCircleEight(e) { let ValueInput = e.target.value; this.setState({ ValueCircleEight : ValueInput}) }
    // UPDATE TITLE GRAPHIC CIRCLE
    UpdateTitleCircleOne(e)   { let TitleInput = e.target.value; this.setState({ TitleCircleOne   : TitleInput }) }
    UpdateTitleCircleTwo(e)   { let TitleInput = e.target.value; this.setState({ TitleCircleTwo   : TitleInput }) }
    UpdateTitleCircleThree(e) { let TitleInput = e.target.value; this.setState({ TitleCircleThree : TitleInput }) }
    UpdateTitleCircleFour(e)  { let TitleInput = e.target.value; this.setState({ TitleCircleFour  : TitleInput }) }
    UpdateTitleCircleFive(e)  { let TitleInput = e.target.value; this.setState({ TitleCircleFive  : TitleInput }) }
    UpdateTitleCircleSix(e)   { let TitleInput = e.target.value; this.setState({ TitleCircleSix   : TitleInput }) }
    UpdateTitleCircleSeven(e) { let TitleInput = e.target.value; this.setState({ TitleCircleSeven : TitleInput }) }
    UpdateTitleCircleEight(e) { let TitleInput = e.target.value; this.setState({ TitleCircleEight : TitleInput }) }

    render() {
        const TestingInput = 'Bar'
        return(
            <>
                {/* <!-- GRAFICA ESTANDAR --> */}
                < TitleGraphic TypeGraphic="standar" TextGraphic="Estandar" />
                
                <div class="container-standar-graphic">
                    
                    < CanvasContainer TypeGraphic="standar" />
                    
                        < InputGraphic 
                            TypeGraphic = "standar" 
                            // METHODS OF THE CREATE GRAPHIC
                            btn = {this.CreateGraphicStandar}
                            // METHODS OF THE UPDATE TITLE
                            inpT1 = {this.UpdateTitleBarOne}
                            inpT2 = {this.UpdateTitleBarTwo}
                            inpT3 = {this.UpdateTitleBarThree}
                            inpT4 = {this.UpdateTitleBarFour}
                            inpT5 = {this.UpdateTitleBarFive}
                            inpT6 = {this.UpdateTitleBarSix}
                            inpT7 = {this.UpdateTitleBarSeven}
                            inpT8 = {this.UpdateTitleBarEight}
                            // METHODS OF THE UPDATE VALUE
                            inpV1 = {this.UpdateValueBarOne}
                            inpV2 = {this.UpdateValueBarTwo}
                            inpV3 = {this.UpdateValueBarThree}
                            inpV4 = {this.UpdateValueBarFour}
                            inpV5 = {this.UpdateValueBarFive}
                            inpV6 = {this.UpdateValueBarSix}
                            inpV7 = {this.UpdateValueBarSeven}
                            inpV8 = {this.UpdateValueBarEight}
                        />
                </div>

                <div class="separation-section"></div>
        
                {/* <!-- GRAFICA CIRCULAR --> */}
                < TitleGraphic TypeGraphic="circle" TextGraphic="Circular" />
                
                <div class="container-circle-graphic">
                    
                    < InputGraphic 
                        TypeGraphic = "circle" 
                        // METHODS OF THE CREATE GRAPHIC
                        btn = {this.CreateGraphicCircle}
                        // METHODS OF THE UPDATE TITLE
                        inpT1 = {this.UpdateTitleCircleOne}
                        inpT2 = {this.UpdateTitleCircleTwo}
                        inpT3 = {this.UpdateTitleCircleThree}
                        inpT4 = {this.UpdateTitleCircleFour}
                        inpT5 = {this.UpdateTitleCircleFive}
                        inpT6 = {this.UpdateTitleCircleSix}
                        inpT7 = {this.UpdateTitleCircleSeven}
                        inpT8 = {this.UpdateTitleCircleEight}
                        // METHODS OF THE UPDATE VALUE
                        inpV1 = {this.UpdateValueCircleOne}
                        inpV2 = {this.UpdateValueCircleTwo}
                        inpV3 = {this.UpdateValueCircleThree}
                        inpV4 = {this.UpdateValueCircleFour}
                        inpV5 = {this.UpdateValueCircleFive}
                        inpV6 = {this.UpdateValueCircleSix}
                        inpV7 = {this.UpdateValueCircleSeven}
                        inpV8 = {this.UpdateValueCircleEight}
                    />
            
                    < CanvasContainer TypeGraphic="circle" />

                </div>
            </>
        )
    }
}

export default PageGraphic;