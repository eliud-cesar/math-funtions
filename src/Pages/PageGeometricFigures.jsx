import React, { Component } from 'react';
import InputOfTheSectionFigures from '../Atoms/Geometric-Figures/InputOfTheSectionFigures';
import OutputOfTheSectionFigures from '../Atoms/Geometric-Figures/OutputOfTheSectionFigures';
import TitleOfTheSectionFigures from '../Atoms/Geometric-Figures/TitlesOfTheSectionFigures';
import TitleAlt from '../Atoms/TitleAlt';
import TitleHead from '../scripts/TitleHead';

class PageGeometricFigures extends Component {
    constructor(props) {
        super(props)

        this.state = {
            InputValueSquareArea: "",
            InputValueSquarePerimeter: "",
            
            InputValueRectangleAreaBase: "",
            InputValueRectangleAreaHeight: "",
            InputValueRectanglePerimeterHeight: "",
            InputValueRectanglePerimeterBase: "",
            
            InputValueCircleArea: "",
            InputValueCirclePerimeter: "",
            
            InputValueTriangleAreaHeight: "",
            InputValueTriangleAreaBase: "",
            InputValueTrianglePerimeter: ""
        }

        this.ChangueValueSquareArea         = this.ChangueValueSquareArea.bind(this)
        this.ChangueValueSquarePerimeter    = this.ChangueValueSquarePerimeter.bind(this)
        
        this.ChangueValueRectangleAreaHeight      = this.ChangueValueRectangleAreaHeight.bind(this)
        this.ChangueValueRectangleAreaBase      = this.ChangueValueRectangleAreaBase.bind(this)
        this.ChangueValueRectanglePerimeterHeight = this.ChangueValueRectanglePerimeterHeight.bind(this)
        this.ChangueValueRectanglePerimeterBase = this.ChangueValueRectanglePerimeterBase.bind(this)
        
        this.ChangueValueCircleArea         = this.ChangueValueCircleArea.bind(this)
        this.ChangueValueCirclePerimeter    = this.ChangueValueCirclePerimeter.bind(this)
        
        this.ChangueValueTriangleAreaHeight       = this.ChangueValueTriangleAreaHeight.bind(this)
        this.ChangueValueTriangleAreaBase       = this.ChangueValueTriangleAreaBase.bind(this)
        this.ChangueValueTrianglePerimeter  = this.ChangueValueTrianglePerimeter.bind(this)
    }

    // REMOVE THE MESSAJE BETA
    componentDidMount() {
        const MessajeBeta = document.getElementById("messaje-beta-container")
        MessajeBeta.classList.remove("active")

        TitleHead("Figuras Geometricas")
    }

    // ---METHOD THAT MAKES THE CALCULATION ONCE THE PAGE IS UPDATED---
    componentDidUpdate() {
        // STATE OF THE INPUTS VALUES
        const { InputValueSquareArea } = this.state
        const { InputValueSquarePerimeter } = this.state
        const { InputValueRectangleAreaBase } = this.state
        const { InputValueRectangleAreaHeight } = this.state
        const { InputValueRectanglePerimeterHeight } = this.state
        const { InputValueRectanglePerimeterBase } = this.state
        const { InputValueCircleArea } = this.state
        const { InputValueCirclePerimeter } = this.state
        const { InputValueTriangleAreaHeight } = this.state
        const { InputValueTriangleAreaBase } = this.state
        const { InputValueTrianglePerimeter } = this.state

        // OUTPUTS
        const OutputAreaSquare = document.getElementById('output-square-area')
        const OutputPerimeterSquare = document.getElementById('output-square-perimeter')
        const OutputAreaRectangle = document.getElementById('output-rectangle-area')
        const OutputPerimeterRectangle = document.getElementById('output-rectangle-perimeter')
        const OutputAreaCircle = document.getElementById('output-circle-area')
        const OutputPerimeterCircle = document.getElementById('output-circle-perimeter')
        const OutputAreaTriangle = document.getElementById('output-triangle-area')
        const OutputPerimeterTriangle = document.getElementById('output-triangle-perimeter')

        // SQUARE
        OutputAreaSquare.innerHTML = InputValueSquareArea * InputValueSquareArea
        OutputPerimeterSquare.innerHTML = InputValueSquarePerimeter * 4
        // RECTANGLE
        OutputAreaRectangle.innerHTML = InputValueRectangleAreaBase * InputValueRectangleAreaHeight
        OutputPerimeterRectangle.innerHTML = (InputValueRectanglePerimeterHeight * 2) + (InputValueRectanglePerimeterBase * 2)
        // CIRCLE
        OutputAreaCircle.innerHTML = Math.PI * (InputValueCircleArea * InputValueCircleArea)
        OutputPerimeterCircle.innerHTML = (Math.PI * 2) * InputValueCirclePerimeter
        // TRIANGLE
        OutputAreaTriangle.innerHTML = (InputValueTriangleAreaHeight * InputValueTriangleAreaBase) / 2
        OutputPerimeterTriangle.innerHTML = InputValueTrianglePerimeter * 3
    }

    // ---FUNCTIONS THAT UPDATE THE STATUS OF THE INPUT THAT ENTERS---
    // SQUARE
    ChangueValueSquareArea(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueSquareArea: TargetValue })
    }
    ChangueValueSquarePerimeter(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueSquarePerimeter: TargetValue })
    }
    // RECTANGLE
    ChangueValueRectangleAreaHeight(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueRectangleAreaHeight: TargetValue })
    }
    ChangueValueRectangleAreaBase(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueRectangleAreaBase: TargetValue })
    }
    ChangueValueRectanglePerimeterHeight(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueRectanglePerimeterHeight: TargetValue })
    }
    ChangueValueRectanglePerimeterBase(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueRectanglePerimeterBase: TargetValue })
    }
    // CIRCLE
    ChangueValueCircleArea(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueCircleArea: TargetValue })
    }
    ChangueValueCirclePerimeter(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueCirclePerimeter: TargetValue })
    }
    // TRIANGLE
    ChangueValueTriangleAreaHeight(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueTriangleAreaHeight: TargetValue })
    }
    ChangueValueTriangleAreaBase(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueTriangleAreaBase: TargetValue })
    }
    ChangueValueTrianglePerimeter(e) {
        const TargetValue = e.target.value
        this.setState({ InputValueTrianglePerimeter: TargetValue })
    }
    
    render() {
        return(
            <>

            <h1 className="title-page-geomtric-figures" >Figuras Geometricas</h1>

            <div className="container-main-grid-figures">
            {/* SQUARE */}
                <div className="container-calculator square">
                    <img src="https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/c1920b227a8e6417faaebd2394479ad8a46d90f3/Geometric%20Figures/Cuadrado.svg" alt={TitleAlt} className="img-square image-figures" />

                    <h3>Cuadrado</h3>

                    <div className="grid-container-calculator square">
                        < TitleOfTheSectionFigures />
                        < InputOfTheSectionFigures Placeholder="lado - area" Funtion={this.ChangueValueSquareArea} />
                        < InputOfTheSectionFigures Placeholder="lado - perimetro" Funtion={this.ChangueValueSquarePerimeter} />
                        < OutputOfTheSectionFigures ClassNameFigure="square" />
                    </div>
                </div>

            {/* RECTANGLE */}
                <div className="container-calculator rectangle">
                    <img src="https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/c1920b227a8e6417faaebd2394479ad8a46d90f3/Geometric%20Figures/Rectangulo.svg" alt={TitleAlt} className="img-rectangle image-figures" />

                    <h3>Rectangulo</h3>

                    <div className="grid-container-calculator rectangle">
                        < TitleOfTheSectionFigures />
                        < InputOfTheSectionFigures Placeholder="altura - area" Funtion={this.ChangueValueRectangleAreaHeight} />
                        < InputOfTheSectionFigures Placeholder="base - area" Funtion={this.ChangueValueRectangleAreaBase} />
                        < InputOfTheSectionFigures Placeholder="altura - perimetro" Funtion={this.ChangueValueRectanglePerimeterHeight} />
                        < InputOfTheSectionFigures Placeholder="base - perimetro" Funtion={this.ChangueValueRectanglePerimeterBase} />
                        < OutputOfTheSectionFigures ClassNameFigure="rectangle" />
                    </div>
                </div>

            {/* CIRCLE */}
                <div className="container-calculator circle">
                    <img src="https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/c1920b227a8e6417faaebd2394479ad8a46d90f3/Geometric%20Figures/Circulo.svg" alt={TitleAlt} className="img-circle image-figures" />

                    <h3>Circulo</h3>

                    <div className="grid-container-calculator circle">
                        < TitleOfTheSectionFigures />
                        < InputOfTheSectionFigures Placeholder="radio - area" Funtion={this.ChangueValueCircleArea} />
                        < InputOfTheSectionFigures Placeholder="radio - perimetro" Funtion={this.ChangueValueCirclePerimeter} />
                        < OutputOfTheSectionFigures ClassNameFigure="circle" />
                    </div>
                </div>

            {/* TRIANGLE */}
                <div className="container-calculator triangle">
                    <img src="https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/c1920b227a8e6417faaebd2394479ad8a46d90f3/Geometric%20Figures/Triangulo.svg" alt={TitleAlt} className="img-triangle image-figures" />

                    <h3>Triangulo</h3>

                    <div className="grid-container-calculator triangle">
                        < TitleOfTheSectionFigures />
                        < InputOfTheSectionFigures Placeholder="altura - area" Funtion={this.ChangueValueTriangleAreaHeight} />
                        < InputOfTheSectionFigures Placeholder="base - area" Funtion={this.ChangueValueTriangleAreaBase} />
                        < InputOfTheSectionFigures Placeholder="lado - perimetro" Funtion={this.ChangueValueTrianglePerimeter} />
                        < OutputOfTheSectionFigures ClassNameFigure="triangle" />
                    </div>
                </div>
            </div>

            </>
        )
    }
}

export default PageGeometricFigures