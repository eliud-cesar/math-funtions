import React, { Component } from 'react'
import GridPagesMaster from '../Organisms/GridPagesMaster'
import TitleHead from '../scripts/TitleHead'

class PageMaster extends Component {
    constructor(props) {
        super(props)
    }

    // REMOVE THE MESSAJE BETA
    componentDidMount() {
        const MessajeBeta = document.getElementById("messaje-beta-container")
        MessajeBeta.classList.remove("active")

        TitleHead("Math Funtions")
    }

    render() {
        return(
            <>
                <h1 class="title-page-master">MATH FUNTIONS</h1>
                <p class="description-page-master">La web que te puede ayudar a aprender y pasar el semestre.</p>

                <GridPagesMaster />
            </>
        )
    }
}

export default PageMaster;