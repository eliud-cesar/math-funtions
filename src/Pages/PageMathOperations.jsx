import axios from 'axios';
import React, { Component } from 'react';
import TitleAlt from '../Atoms/TitleAlt'
import Information from '../Molecules/Math-Operations/Information';
import TitleHead from '../scripts/TitleHead';

const LinkIconIgual = "https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/b3e65a5c9c5d40ab8639f808f1eb2d3e2df1801e/Icons/Igual.svg"

class PageMathFuntions extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // THE STATE WHERE THE API DATA IS STORED WITHOUT BEING MODIFIED
            dataMathOp: [],
            // THE STATE WHERE THE DATA IS STORED THAT THE INFORMATION WILL BE DISPLAYED
            dataApiDinamic: [],
            // THE STATES WHERE THE VALUE OF THE INPUTS WILL BE STORED
            valueInputOne: 0,
            valueInputTwo: 0
        }

        // METHODS
        this.optionMathOperation   = this.optionMathOperation.bind(this)
        this.opertationSumar       = this.opertationSumar.bind(this)
        this.opertationRestar      = this.opertationRestar.bind(this)
        this.opertationMultiplicar = this.opertationMultiplicar.bind(this)
        this.opertationDividir     = this.opertationDividir.bind(this)
        this.inputValueOne         = this.inputValueOne.bind(this)
        this.inputValueTwo         = this.inputValueTwo.bind(this)
    }

    // PETITION FOR API OF INFORMATION
    componentDidMount() {
        {axios.get("https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/master/Apis/Math-Operations.json")
            .then(respData => {
                this.setState ({
                    dataMathOp: respData.data
                })
            })}

        // REMOVE THE MESSAJE BETA
        {
            const MessajeBeta = document.getElementById("messaje-beta-container")
            MessajeBeta.classList.remove("active")
        }

        TitleHead("Operaciones Matematicas")
    }


    // METHODS WHERE THE VALUE OF THE INPUT IS SAVED EACH TIME THE VALUE IS CHANGED
    inputValueOne(e) {
        const ValorInputOne = e.target.value
        this.setState({valueInputOne: ValorInputOne})
    }
    inputValueTwo(e) {
        const ValorInputTwo = e.target.value
        this.setState({valueInputTwo: ValorInputTwo})
    }

    // ADD THE VALUES AND PRINT THEM
    opertationSumar() {
        const { valueInputOne } = this.state
        const { valueInputTwo } = this.state
        const OutputOp = document.getElementById('output-operation')

        OutputOp.innerText = `${parseFloat(valueInputOne) + parseFloat(valueInputTwo)}`
    }

    // SUBTRACT THE VALUES AND PRINT THEM
    opertationRestar() {
        const { valueInputOne } = this.state
        const { valueInputTwo } = this.state
        const OutputOp = document.getElementById('output-operation')

        OutputOp.innerText = `${parseFloat(valueInputOne) - parseFloat(valueInputTwo)}`
    }

    // MULTIPLY THE VALUES AND PRINT THEM
    opertationMultiplicar() {
        const { valueInputOne } = this.state
        const { valueInputTwo } = this.state
        const OutputOp = document.getElementById('output-operation')

        OutputOp.innerText = `${parseFloat(valueInputOne) * parseFloat(valueInputTwo)}`
    }

    // DIVIDE THE VALUES AND PRINT THEM
    opertationDividir() {
        const { valueInputOne } = this.state
        const { valueInputTwo } = this.state
        const OutputOp = document.getElementById('output-operation')

        {
            ((parseFloat(valueInputOne) / parseFloat(valueInputTwo)) === Infinity) 
            ? OutputOp.innerText = `Error`
            : OutputOp.innerText = `${parseFloat(valueInputOne) / parseFloat(valueInputTwo)}`
        }
    }


    // MENU METHOD THAT VALIDATES ON WHICH OPERATION IS CHOSEN AND FROM THERE THE OTHER IMPRESSIONS ARE DERIVED
    optionMathOperation(e) {

        // HERE YOU GET THE VALUE AND THE TEXT - THANKS TO STACKOVERFLOW
        let index = e.target.selectedIndex;
        const TextOption = e.target.options[index].text
        const ValueOption = e.target.options[index].value
        
        // THE ARRAY OF THE STATE TO WHICH LEAKED DATA IS SENT
        const { dataMathOp } = this.state

        // THE CONTAINER IS OBTAINED FROM:
        // - OPERATION SYMBOL
        const Simbolo = document.getElementById('container-signal-operartor')
        // - SUBMIT BUTTON
        const Submit = document.getElementById('container-button-start-operation')


        // THE VALIDATION ON WHETHER THE 2 CONDITIONS ARE FULFILLED, THE VALUE AND THE TEXT
        switch(TextOption && ValueOption) {

            // ADD
            case 'Suma' && 'S':
                // CONSOLE
                console.log("1")
                // THE SIGN OF THE OPERATION IS PRINTED
                Simbolo.innerText = '+'
                // THE DEFAULT BUTTON IS REPLACED AND THE INPUT IS PRINTED WITH AN ID FOR THE OPERATION METHOD
                Submit.innerHTML = `<input id="button-ejecut-sumar" type="submit" value="Sumar" />`
                // THE ID IS OBTAINED AND THE "CLICK" EVENT IS HEARD FOR THIS TO EXECUTE ITS METHOD REGARDING THE OPERATION
                const ButtonSumar = document.getElementById('button-ejecut-sumar')
                ButtonSumar.onclick = () => this.opertationSumar()
                // UPDATING STATUS
                const ObjetSetInformations = dataMathOp[0]
                this.setState ({
                    dataApiDinamic: [ObjetSetInformations]
                })
                break
                
            // SUBTRACT
            case 'Resta' && 'R': 
                // CONSOLE
                console.log("2")
                // THE SIGN OF THE OPERATION IS PRINTED
                Simbolo.innerText = '-'
                // THE DEFAULT BUTTON IS REPLACED AND THE INPUT IS PRINTED WITH AN ID FOR THE OPERATION METHOD
                Submit.innerHTML = `<input id="button-ejecut-restar" type="submit" value="Restar"/>`
                // THE ID IS OBTAINED AND THE "CLICK" EVENT IS HEARD FOR THIS TO EXECUTE ITS METHOD REGARDING THE OPERATION
                const ButtonRestar = document.getElementById('button-ejecut-restar')
                ButtonRestar.onclick = () => this.opertationRestar()
                // UPDATING STATUS
                const ObjetSetInformationR = dataMathOp[1]
                this.setState ({
                    dataApiDinamic: [ObjetSetInformationR]
                })
                break
            
            // MULTIPLY
            case 'Multiplicacion' && 'M': 
                // CONSOLE
                console.log("3")
                // THE SIGN OF THE OPERATION IS PRINTED
                Simbolo.innerText = 'x'
                // THE DEFAULT BUTTON IS REPLACED AND THE INPUT IS PRINTED WITH AN ID FOR THE OPERATION METHOD
                Submit.innerHTML = `<input id="button-ejecut-multiplicar" type="submit" value="Multiplicar"/>`
                // THE ID IS OBTAINED AND THE "CLICK" EVENT IS HEARD FOR THIS TO EXECUTE ITS METHOD REGARDING THE OPERATION
                const ButtonMultlipicar = document.getElementById('button-ejecut-multiplicar')
                ButtonMultlipicar.onclick = () => this.opertationMultiplicar()
                // UPDATING STATUS
                const ObjetSetInformationM = dataMathOp[2]
                this.setState ({
                    dataApiDinamic: [ObjetSetInformationM]
                })
                break
            
            // DIVIDI
            case 'Division' && 'D': 
                // CONSOLE
                console.log("4")
                // THE SIGN OF THE OPERATION IS PRINTED
                Simbolo.innerText = '/'
                // THE DEFAULT BUTTON IS REPLACED AND THE INPUT IS PRINTED WITH AN ID FOR THE OPERATION METHOD
                Submit.innerHTML = `<input id="button-ejecut-dividir" type="submit" value="Dividir"/>`
                // THE ID IS OBTAINED AND THE "CLICK" EVENT IS HEARD FOR THIS TO EXECUTE ITS METHOD REGARDING THE OPERATION
                const ButtonDividir = document.getElementById('button-ejecut-dividir')
                ButtonDividir.onclick = () => this.opertationDividir()
                // UPDATING STATUS
                const ObjetSetInformationD = dataMathOp[3]
                this.setState ({
                    dataApiDinamic: [ObjetSetInformationD]
                })
                break
        }
    }


    render() {
        // THE STATUS OF THE API IS BRINGED WHERE THE INFORMATION DATA IS SAVED
        const { dataApiDinamic } = this.state
        return(
        <>
            <h1 className="title-page-math-operations">Operaciones Matematicas</h1>

            {/* OPTIONS */}
            <div className="container-options-operations">
                <h2>Opciones</h2>
                <select name="provincia" id="provincia" onChange={this.optionMathOperation}>
                    <option value="" disabled selected>Seleccione una Operación...</option>
                    <option value="S">Suma</option>
                    <option value="R">Resta</option>
                    <option value="M">Multiplicacion</option>
                    <option value="D">Division</option>
                </select>
            </div>

            {/* SECTION OF THE OPERATION */}
            <div className="container-section-operations">
                <input type="number" name="" id="input-one" onChange={this.inputValueOne} />
                <div className="container-signal-operartor" id="container-signal-operartor"></div>
                <input type="number" name="" id="input-two" onChange={this.inputValueTwo} />

                <div className="container-img-igual-operator">
                    <img src={LinkIconIgual} alt={ TitleAlt } />
                </div>
                
                <div className="result-output-math" id="output-operation"></div>
            </div>

            {/* BUTTON */}
            <div id="container-button-start-operation" className="container-button-start-operation" >
                <input id="button-ejecut" type="submit" value="Ejecutar" />
            </div>
            
            {/* INFORMATION */}
            <div id="information-operator-math" className="information-operator-math">
                {
                    // VALIDATION IF THE SECONDARY STATE HAS DATA, TO HAVE A DEFAULT
                    (dataApiDinamic.length == []) 
                    ? <Information 
                        Title="Titulo de la operacion"
                        Description="Descripccion de la operacion"
                    />
                    : dataApiDinamic.map(u => (
                        <Information 
                            Title={u.Title}
                            Description={u.Description}
                        />
                    ))
                }
            </div>
        </>
        )
    }
}

export default PageMathFuntions;