import React, { Component } from 'react';
import TitleAlt from '../Atoms/TitleAlt';
import Image from '../Images/Page-Not-Found.svg'
import TitleHead from '../scripts/TitleHead';

class PageNotFound extends Component {
    constructor(props) {
        super(props)
    }

    // REMOVE THE MESSAJE BETA
    componentDidMount() {
        const MessajeBeta = document.getElementById("messaje-beta-container")
        MessajeBeta.classList.remove("active")

        TitleHead("Pagina no encontrada")
    }

    render() {
        return(
    <>
    <h1 className="title-404" >Pagina No Encontrada</h1>

    <div className="container-image-404">
        <img className="image-404" src={Image} alt={TitleAlt} />
    </div>

    <p className="description-404" >Hiciste un mal calculo porque el resultado esta mal... 📐</p>
    </>
        )
    }
}

export default PageNotFound;