import axios from 'axios';
import React, { Component } from 'react';
import CardMasterPresentacion from '../Molecules/Master/CardMasterPresentacion';

class GridPagesMaster extends Component {
    constructor(props) {
        super(props)

        this.state = {
            dataCard: []
        }
    }

    // PETITION OF API
    componentDidMount() {
        axios.get("https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/master/Apis/Master.json")
            .then(resp  => {
                this.setState({
                    dataCard: resp.data
                })
            })
    }

    render() {
        const { dataCard } = this.state
        console.log(dataCard)
        return(
            <div class="grid-pages-master">
                {
                    dataCard.map(u => (
                        <CardMasterPresentacion 
                            Image={u.Image === "" ? "https://raw.githubusercontent.com/eliud-cesar/database-math-funtins/master/anime.jpg" : u.Image}
                            Title={u.Title}
                            Description={u.Description}
                            Link={u.Link}
                        />
                    ))
                }
            </div>
        )
    }
}

export default GridPagesMaster;