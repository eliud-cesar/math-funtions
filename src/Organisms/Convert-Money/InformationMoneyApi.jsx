import React from 'react';
import InformationMoney from '../../Molecules/Convert-Money/InformationMoney';

const InformationMoneyApi = ({ ApiInformationOne, ApiInformationTwo }) => (
    <>
        <div class="cards-iformation">
            {
                (ApiInformationOne.length == []) 
                ? < InformationMoney 
                    Title="Titulo" 
                    Description="Descripccion" 
                    ImageUrl="https://cdn.pixabay.com/photo/2017/08/30/07/56/money-2696229_960_720.jpg" 
                />
                : ApiInformationOne.map(u => (
                    < InformationMoney 
                        Title={u.Title}
                        Description={u.Description}
                        ImageUrl={u.image}
                    />
                ))
            }
            {
                ApiInformationTwo.length == [] 
                ? < InformationMoney 
                    Title="Titulo" 
                    Description="Descripccion" 
                    ImageUrl="https://cdn.pixabay.com/photo/2017/08/30/07/56/money-2696229_960_720.jpg" 
                />
                : ApiInformationTwo.map(u => (
                    < InformationMoney 
                        Title={u.Title}
                        Description={u.Description}
                        ImageUrl={u.image}
                    />
                ))
            }
        </div>
    </>
)

export default InformationMoneyApi;