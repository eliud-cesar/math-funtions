// REACT.JS
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import React from 'react';

// GENERALS
import Header from './Molecules/Header';
import Footer from './Molecules/Footer';

// STYLES
import './Styles/styles-web.css'

// PAGES
import PageMaster from './Pages/PageMaster';
import PageMathFuntions from './Pages/PageMathOperations';
import PageConvertirDinero from './Pages/PageConvertirDinero';
import PageGeometricFigures from './Pages/PageGeometricFigures';
import PageGraphic from './Pages/PageGraphic';
import MessajeBeta from './Atoms/MessajeBeta'

// PAGE NOT FOUNT
import PageNotFound from './Pages/PageNotFound';

const App = () => (
  <Router>

    <Header/>

    {/* ANUNCIO BETA */}
    < MessajeBeta />

    <main className="main mathfuntions">

    <Switch>

      <Route path="/" exact component={ PageMaster } />
      <Route path="/operaciones-matematicas" exact component={ PageMathFuntions } />
      <Route path="/convertir-dinero" exact component={ PageConvertirDinero } />
      <Route path="/figuras-geometricas" exact component={ PageGeometricFigures } />
      <Route path="/grafica" exact component={ PageGraphic } />
      <Route component={ PageNotFound } />

    </Switch>
    </main>

    <Footer/>

  </Router>
)

export default App;
